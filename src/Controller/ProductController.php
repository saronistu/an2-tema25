<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = new Product();
        $product -> setName('Keyboard');
        $product -> setPrice(120);
        $product -> setDescription("Wireless and RGB.");

        $entityManager->persist($product);
        $entityManager->flush();

        return $this->render('product/index.html.twig', [
            'controller_name' => $product->getId(),
        ]);
    }

   /**
    * @Route("/show/{id}", name="show")
    */
    public function show($id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        if (!$product) {
            throw $this->createNotFoundException('No product found for id ' . $id);
        }
        return $this->render('product/index.html.twig', ['controller_name' => $product->getName()]);
    }

    /**
    * @Route("/product/update/{id}", name="update")
    */
    public function update($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
    
        $product->setName('New product name!');
        $entityManager->flush();
    
        return $this->redirectToRoute('product_show', [
            'id' => $product->getId()
        ]);
    }

    /**
    * @Route("/product/delete/{id}", name="delete")
    */
    public function delete($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $entityManager->remove($product);
        $entityManager->flush();    

        return $this->redirectToRoute('show', ['id' => $product->getId()
        ]);
    }
}
